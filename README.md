## Opis

Konsolowa wersja gry [Magic Square](https://play.google.com/store/apps/details?id=com.littletrickstudio.derek.chinesemagicsquare&hl=pl).

Przykład:

```
     6   15  7    15
     -----------
3:  | 2 | 7 | 6 | 15
     -----------  
2:  |   | 5 | 1 | 6
     -----------  
1:  | 4 | 3 |   | 7
     -----------
      A   B   C   7

Brakujące: 8 9
Twój ruch: c1 8




     6   15  15   15
     -----------
3:  | 2 | 7 | 6 | 15
     -----------  
2:  |   | 5 | 1 | 6
     -----------  
1:  | 4 | 3 | 8 | 15
     -----------
      A   B   C   15

Brakujące: 9
Twój ruch:
```

## ToDo

1. Testy, testy, testy, ...
2. Przebieg gry.
3. Baza kwadratów (a może generator?).
4. Generowanie kwadratów z istniejących.
5. Baza etapów.
6. Generowanie losowych etapów.
